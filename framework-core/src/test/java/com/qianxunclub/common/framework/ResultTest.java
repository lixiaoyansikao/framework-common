package com.qianxunclub.common.framework;

import com.qianxunclub.common.framework.constant.CommonReturnCode;
import com.qianxunclub.common.framework.response.Result;
import org.junit.Test;

public class ResultTest {

    /**
     * 使用泛型返回结果
     */
    @Test
    public void test(){
        Result<TestMode> result = Result.result(new TestMode("244324")).setCode(CommonReturnCode.EXITS);
        System.out.println("equals：" + result.equals(CommonReturnCode.OK));
        System.out.println("toString：" + result.toString());
    }

    /**
     * 默认返回数据
     */
    @Test
    public void defaultTest(){
        Result result = new Result<>();
        result.setResult("测试数据");
        System.out.println("默认返回数据：" + result.toString());
    }

    /**
     * 使用通用返回编码
     */
    @Test
    public void commonReturnCodeTest(){
        Result result = new Result<>();
        result.setCode(CommonReturnCode.EXITS);
        System.out.println("使用通用返回编码：" + result.toString());
    }

    /**
     * 重新定义返回信息
     */
    @Test
    public void msgTest(){
        Result result = new Result<>();
        result.setCode(CommonReturnCode.EXITS).setMessage("重新定义返回信息");
        System.out.println("使用通用返回编码：" + result.toString());
    }

    /**
     * 使用自定义返回编码
     */
    @Test
    public void customReturnCodeTest(){
        Result result = new Result<>();
        result.setCode(CustomReturnCode.BHS001);
        System.out.println("使用自定义返回编码：" + result.toString());
    }

    /**
     * 使用泛型返回结果
     */
    @Test
    public void genericTest(){
        Result<TestMode> result = new Result<>();
        TestMode testMode = new TestMode("姓名");
        result.setResult(testMode);
        System.out.println("使用泛型返回结果：" + result.toString());
    }

    /**
     * 使用泛型返回结果
     */
    @Test
    public void commonTest(){
        Result result = new Result<>();
        System.out.println("equals：" + result.equals(CommonReturnCode.OK));
        System.out.println("toString：" + result.toString());
    }

    /**
     * 测试使用的mode
     */
    class TestMode{
        private String name;

        public TestMode(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }


}
