package com.qianxunclub.common.framework;

import com.qianxunclub.common.framework.response.ReturnCode;

/**
 * 自定义返回编码
 * @author chihiro.zhang
 */

public enum CustomReturnCode implements ReturnCode {

    /**
     * 自定义错误编码
     */
    BHS001("BHS001", "自定义错误编码");

    /**
     * 业务编号
     */
    private String code;

    /**
     * 业务值
     */
    private String message;

    /**
     *
     * @param code 错误编码
     * @param message 错误描述
     */
    CustomReturnCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }

}
