package com.qianxunclub.common.framework.response;


import com.qianxunclub.common.framework.constant.CommonReturnCode;

import java.io.Serializable;



/**
 * 如果需要自定义错误编码，需要继承ReturnCode接口
 * @see com.qianxunclub.common.framework.response.ReturnCode
 * @author chihiro.zhang
 */
public class Result<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String code = CommonReturnCode.OK.code();
	
	private String message = CommonReturnCode.OK.message();

    private T result;



    public static<T> Result<T> result(T result){
        Result<T> resultStatus = new Result<>();
		resultStatus.setResult(result);
		return resultStatus;
	}

	public static<T> Result<T> ok(){
		Result<T> resultStatus = new Result<>();
		return resultStatus;
	}

    public static<T> Result<T> code(ReturnCode returnCode){
        Result<T> resultStatus = new Result<>();
        if(returnCode != null){
            resultStatus.code = returnCode.code();
            resultStatus.message = returnCode.message();
        }
        return resultStatus;
    }

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public Result<T> setMessage(String message) {
		this.message = message;
		return this;
	}

	public T getResult() {
		return result;
	}

	public Result<T> setResult(T result) {
		this.result = result;
		return this;
	}

	public Result<T> setCode(ReturnCode returnCode){
		if(returnCode != null){
			this.code = returnCode.code();
			this.message = returnCode.message();
		}
		return this;
	}

	/**
	 * 方法对比的是code的值
	 * @param returnCode
	 * @return
	 */
	public boolean equals(ReturnCode returnCode){
		if(returnCode != null){
			return this.code.equals(returnCode.code());
		}
		return Boolean.FALSE;
	}

}
