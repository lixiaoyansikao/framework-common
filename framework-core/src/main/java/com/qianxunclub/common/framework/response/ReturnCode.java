package com.qianxunclub.common.framework.response;

/**
 * 如果需要自定义错误编码，需要继承ReturnCode接口
 * @see com.qianxunclub.common.framework.response.ReturnCode
 * @author chihiro.zhang
 */
public interface ReturnCode {

    /**
     * 获取返回编码
     * @return code
     */
    String code();

    /**
     * 获取返回描述信息
     * @return message
     */
    String message();

}
