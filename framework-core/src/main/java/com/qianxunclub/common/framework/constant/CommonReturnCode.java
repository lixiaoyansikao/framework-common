package com.qianxunclub.common.framework.constant;

import com.qianxunclub.common.framework.response.ReturnCode;

/**
 * 如果需要自定义错误编码，需要继承ReturnCode接口
 * @see com.qianxunclub.common.framework.response.ReturnCode
 * @author chihiro.zhang
 */
public enum CommonReturnCode implements ReturnCode {
    /**
     *  数据处理成功
     */
    OK("OK", "success!"),
    /**
     * 数据处理失败，如：保存、发送
     */
    FAIL("SYS001", "processing faile!"),
    /**
     * 数据信息不存在
     */
    NOT_EXITS("SYS002", "Data does not exist!"),

    /**
     * 数据重复
     */
    EXITS("SYS003", "The data has already existed!"),
    /**
     * 未知错误
     */
    UNKNOW("SYS004", "unknown error!"),
    /**
     * 异常
     */
    EXCEPTION("SYS005", "Server handling exceptions!"),
    /**
     * 用户输入或接口入参缺少
     */
    PARAM_ERROR("SYS006", "Parameter error!"),
    /**
     * 系统应用间通讯超时
     */
    TIMEOUT("SYS007", "timeout!");

    /**
     * 业务编号
     */
    private String code;

    /**
     * 业务值
     */
    private String message;

    CommonReturnCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String code() {
        return code;
    }


    @Override
    public String message() {
        return message;
    }


}
