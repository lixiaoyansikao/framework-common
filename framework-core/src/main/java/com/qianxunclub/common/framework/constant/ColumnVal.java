package com.qianxunclub.common.framework.constant;

/**
 * @author chihiro.zhang
 */
public class ColumnVal {

    public static final int YES = 1;
    public static final int NO = 0;

    public class ENABLED{

        public static final int YES = 1;
        public static final int NO = 0;
    }

    public class DELETED{
        public static final int YES = 1;
        public static final int NO = 0;
    }
}

