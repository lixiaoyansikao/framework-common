package com.qianxunclub.common.tools.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author chihiro.zhang
 */
public class HttpUtil {

    private static Logger log = LoggerFactory.getLogger(HttpUtil.class);

    protected HttpUtil() {

    }

    /**
     * 连接时间
     */
    private static int connectTimeout = 30000;

    /**
     * 响应时间
     */
    private static int readTimeout = 60000;

    protected static HttpClient mClient;


    public static String post(String url, Map<String, String> header, Map<String, Object> body, File in, String fileName)
            throws IOException {
        mClient = new DefaultHttpClient();
        mClient.getParams().setParameter("http.socket.timeout", 5 * 1000);
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("accept", "*/*");
        httpPost.setHeader("connection", "Keep-Alive");
        httpPost.setHeader("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
        if (null != header) {
            for (Map.Entry<String, String> entry : header.entrySet()) {
                httpPost.setHeader(entry.getKey(), entry.getValue());
            }
        }

        if (null != header && (false == header.containsKey("Content-Type")
                || header.get("Content-Type").equals("multipart/form-storage"))) {
            MultipartEntity multipartEntity = new MultipartEntity();
            if (body != null) {
                for (Map.Entry<String, Object> entry : body.entrySet()) {
                    multipartEntity.addPart(entry.getKey(), new StringBody(entry.getValue().toString(), ContentType.MULTIPART_FORM_DATA));
                }
            }

            if (in != null) {
                ContentBody contentBody = new FileBody(in, ContentType.MULTIPART_FORM_DATA);
                multipartEntity.addPart(fileName, contentBody);
            }
            httpPost.setEntity(multipartEntity);
        } else {
            if (in != null) {
                String strBody = new String(in.toString());
                StringEntity stringEntity = new StringEntity(strBody);
                httpPost.setEntity(stringEntity);
            }
        }

        HttpResponse httpResponse = mClient.execute(httpPost);
        return EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
    }

    /**
     * MAP数据转换成HTTP参数格式的字符串
     *
     * @param requestMap
     * @return String
     */
    public static String toHttpParamString(Map<String, Object> requestMap) {
        StringBuilder result = new StringBuilder();
        if (requestMap != null && !requestMap.isEmpty()) {
            for (Map.Entry<String, Object> entry : requestMap.entrySet()) {
                if (!StringUtils.isEmpty(entry.getValue().toString())) {
                    result.append(entry.getKey() + "=" + entry.getValue() + "&");
                }
            }
            result = new StringBuilder(result.substring(0, result.length() - 1));
        }
        return result.toString();
    }

    /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url   发送请求的 URL
     * @param param 请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return 所代表远程资源的响应结果
     */
    public static String sendGet(String url, String param) throws Exception {
        return sendGet(url, param, "application/x-www-form-urlencoded");
    }

    /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url   发送请求的 URL
     * @param param 请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return 所代表远程资源的响应结果
     */
    public static String sendJsonGet(String url, String param) throws Exception {
        return sendGet(url, param, "application/json");
    }

    /**
     * 向指定URL发送GET方法的请求
     *
     * @param url         发送请求的URL
     * @param param       请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @param contentType 媒体类型
     * @return URL 所代表远程资源的响应结果
     */
    public static String sendGet(String url, String param, String contentType) throws Exception {
        return sendGet(url, param, contentType, connectTimeout, readTimeout);
    }

    /**
     * 向指定URL发送GET方法的请求
     *
     * @param url            发送请求的URL
     * @param param          请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @param contentType    媒体类型
     * @param connectTimeout 连接时间
     * @param readTimeout    响应时间
     * @return URL 所代表远程资源的响应结果
     */
    public static String sendGet(String url, String param, String contentType, int connectTimeout, int readTimeout) throws Exception {
        String result = "";
        BufferedReader in = null;
        try {
            if (StringUtils.isNotEmpty(param)) {
                if (url.indexOf("?") > 0) {
                    url = url + "&" + param;
                } else {
                    url = url + "?" + param;
                }
            }
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            conn.setRequestProperty("Content-Type", contentType);
            conn.setConnectTimeout(connectTimeout);
            conn.setReadTimeout(readTimeout);
            // 建立实际的连接
            conn.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = conn.getHeaderFields();
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            result = "ERROR";
            throw new Exception("sendGet method error: " + e);
        } finally {
            // 使用finally块来关闭输入流
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e) {
                log.error("sendGet method close BufferedReader " + e);
            }
        }
        return result;
    }
    /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url   发送请求的 URL
     * @param param 请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return 所代表远程资源的响应结果
     */
    public static String sendPost(String url, String param) throws Exception {
        return sendPost(url, param, "application/x-www-form-urlencoded");
    }

    /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url   发送请求的 URL
     * @param param 请求参数
     * @return 所代表远程资源的响应结果
     */
    public static String sendJsonPost(String url, String param) throws Exception {
        return sendPost(url, param, "application/json");
    }

    /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url         发送请求的 URL
     * @param param       请求参数
     * @param contentType 媒体类型
     * @return 所代表远程资源的响应结果
     */
    public static String sendPost(String url, String param, String contentType) throws Exception {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Content-Type", contentType);
        return sendPost(url, param, connectTimeout, readTimeout, headerMap);
    }

    /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url            发送请求的 URL
     * @param param          请求参数
     * @param contentType    媒体类型
     * @param connectTimeout 连接时间
     * @param readTimeout    响应时间
     * @return 所代表远程资源的响应结果
     */
    public static String sendPost(String url, String param, String contentType, int connectTimeout, int readTimeout) throws Exception {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Content-Type", contentType);
        return sendPost(url, param, connectTimeout, readTimeout, headerMap);
    }

    /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url       发送请求的 URL
     * @param param     请求参数
     * @param headerMap 请求头
     * @return 所代表远程资源的响应结果
     */
    public static String sendJsonPost(String url, String param, Map<String, String> headerMap) throws Exception {
        if (null == headerMap) {
            headerMap = new HashMap<String, String>();
        }
        headerMap.put("Content-Type", "application/json");
        return sendPost(url, param, connectTimeout, readTimeout, headerMap);
    }

    /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url            发送请求的 URL
     * @param param          请求参数
     * @param connectTimeout 连接时间
     * @param readTimeout    响应时间
     * @param headerMap      请求头
     * @return 所代表远程资源的响应结果
     */
    public static String sendJsonPost(String url, String param, int connectTimeout, int readTimeout, Map<String, String> headerMap) throws Exception {
        if (null == headerMap) {
            headerMap = new HashMap<String, String>();
        }
        headerMap.put("Content-Type", "application/json");
        return sendPost(url, param, connectTimeout, readTimeout, headerMap);
    }

    /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url            发送请求的 URL
     * @param param          请求参数
     * @param connectTimeout 连接时间
     * @param readTimeout    响应时间
     * @param headerMap      请求头
     * @return 所代表远程资源的响应结果
     */
    public static String sendPost(String url, String param, int connectTimeout, int readTimeout, Map<String, String> headerMap) throws Exception {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            if (null != headerMap && !headerMap.isEmpty()) {
                for (Map.Entry<String, String> entry : headerMap.entrySet()) {
                    if (StringUtils.isNotEmpty(entry.getValue())) {
                        conn.setRequestProperty(entry.getKey(), entry.getValue());
                    }
                }
            }
            conn.setConnectTimeout(connectTimeout);
            conn.setReadTimeout(readTimeout);
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            result = "ERROR";
            throw new Exception("sendPost method error: " + e);
        } finally {
            //使用finally块来关闭输出流、输入流
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                log.error("sendPost method close PrintWriter or BufferedReader " + ex);
            }
        }
        return result;
    }

    /**
     * 发送HTTP POST请求
     *
     * @param url    发送请求的 URL
     * @param params 请求参数
     * @return 所代表远程资源的响应结果
     */
    public static String httpPost(String url, Map<String, Object> params) throws Exception {
        // 构建请求参数
        String paramStr = toHttpParamString(params);
        return httpPost(url, paramStr, "application/x-www-form-urlencoded","POST",connectTimeout, readTimeout);
    }

    public static String httpPost(String url, String params, String contentType) throws Exception {
        return httpPost(url,params,contentType,"POST",connectTimeout, readTimeout);
    }

    public static String sendHttp(String url, String params, String contentType,String method) throws Exception {
        return httpPost(url,params,contentType,method,connectTimeout, readTimeout);
    }

    /**
     * 发送HTTP POST请求
     *
     * @param url            发送请求的 URL
     * @param params         请求参数
     * @param connectTimeout 连接时间
     * @param readTimeout    响应时间
     * @return 所代表远程资源的响应结果
     */
    public static String httpPost(String url, String params,String contentType,String method, int connectTimeout, int readTimeout) throws Exception {
        // 读取返回内容
        StringBuilder buffer = new StringBuilder();
        // 尝试发送请求
        HttpURLConnection con = null;
        OutputStreamWriter osw = null;
        BufferedReader in = null;
        try {
            URL realUrl = new URL(url);
            con = (HttpURLConnection) realUrl.openConnection();
            // POST 只能为大写，严格限制，post会不识别
            con.setRequestMethod(method);
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setUseCaches(false);
            con.setRequestProperty("Content-Type", contentType);
            con.setConnectTimeout(connectTimeout);
            con.setReadTimeout(readTimeout);
            if(params != null) {
                con.getOutputStream().write(params.getBytes("UTF-8"));
            }
            // flush输出流的缓冲
            con.getOutputStream().flush();
            //一定要有返回值，否则无法把请求发送给server端。
            in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
            String temp;
            while ((temp = in.readLine()) != null) {
                buffer.append(temp);
                buffer.append("\n");
            }
        } catch (Exception e) {
            buffer = new StringBuilder("ERROR");
            throw new Exception("httpPost method error: " + e);
        } finally {
            //使用finally块来关闭连接、输出流、输入流
            try {
                if (osw != null) {
                    osw.close();
                }
                if (in != null) {
                    in.close();
                }
                if (con != null) {
                    con.disconnect();
                }
            } catch (IOException ex) {
                log.error("httpPost method close OutputStreamWriter or BufferedReader or HttpURLConnection " + ex);
            }
        }
        return buffer.toString();
    }


}
