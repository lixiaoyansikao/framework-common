package com.qianxunclub.common.tools.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * JsonUtil工具类
 * 导入com.fasterxml.jackson.core.jackson-databind.jar包
 *
 * @author HJ
 * @ClassName: JsonUtil
 * @date 2016-3-16
 */
public class JsonUtil {

    protected JsonUtil(){

    }

    private static ObjectMapper JSON_MAPPER = new ObjectMapper();

    public static <T> String objectToJson(T data) {
        String json = null;
        if (data != null) {
            try {
                json = JSON_MAPPER.writeValueAsString(data);
            } catch (Exception e) {
                try {
                    throw new Exception("objectToJson method error: " + e);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
        return json;
    }

    public static <T> T jsonToObject(String json, Class<T> cls) {
        T object = null;
        if (StringUtils.isNotEmpty(json) && cls != null) {
            try {
                if (json.startsWith("\"{")) {
                    json = json.replace("\"{", "{").replace("}\"", "}").replace("\\\"", "\"");
                }
                object = JSON_MAPPER.readValue(json, cls);
            } catch (Exception e) {
                try {
                    throw new Exception("jsonToObject method error: " + e);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
        return object;
    }

    public static List<Map<String, Object>> jsonToList(String json) {
        List<Map<String, Object>> list = null;
        if (StringUtils.isNotEmpty(json)) {
            try {
                if (json.startsWith("\"[")) {
                    json = json.replace("\"[", "[").replace("]\"", "]").replace("\\\"", "\"");
                }
                list = JSON_MAPPER.readValue(json, List.class);
            } catch (Exception e) {
                try {
                    throw new Exception("jsonToList method error: " + e);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
        return list;
    }

    public static Map<String, Object> jsonToMap(String json) {
        Map<String, Object> maps = null;
        if (StringUtils.isNotEmpty(json)) {
            try {
                if (json.startsWith("\"{")) {
                    json = json.replace("\"{", "{").replace("}\"", "}").replace("\\\"", "\"");
                }
                maps = JSON_MAPPER.readValue(json, Map.class);
            } catch (Exception e) {
                try {
                    throw new Exception("jsonToMap method error: " + e);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
        return maps;
    }
}
